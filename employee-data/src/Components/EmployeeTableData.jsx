import React from 'react'


export default function EmployeeTableData({employeeData, firstName, lastName}) {
  return (
    
 <div className='employees'>
      <h1 className='table-title'>Employee Data</h1>
      <div className='table-data'>
      <table>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
            {employeeData.map((data) => (
      
              <tr>
                <td>{data.firstName}</td>
                <td>{data.lastName}</td>
              </tr>
     
            ))}        
        </tbody>
        </table>
          </div>
</div>
    
  )
}

