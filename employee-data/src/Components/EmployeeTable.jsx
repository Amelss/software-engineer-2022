import React from 'react'
import { useState, useEffect  } from 'react'
import EmployeeTableData from './EmployeeTableData';
import axios from 'axios';



export default function EmployeeTable() {

  const [employeeData, setEmployeeData] = useState([]);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('')
 
  

 const getEmployeeData = async () => {
   await axios
     .get("http://localhost:3000/employees")
     .then((res) => {
       setEmployeeData(res.data);
       console.log(res.data);
     })
     .catch((err) => {
       console.log(err);
     });
 };
  
  const addNewEmployee = async (newEmployee) => {
    await axios
      .post("http://localhost:3000/employees", newEmployee)
      .then((res) => {
        console.log(res);
        getEmployeeData();
      })
      .catch((err) => {
        console.log(err);
      });
  };  
  
  
   const handleSubmit = (e) => {
    e.preventDefault();
    const createEmployee = {firstName, lastName}
    addNewEmployee(createEmployee);
   
    console.log(createEmployee)
  
  } 

 useEffect(() => {
   getEmployeeData();
 }, []);
  




  return (
    <div >

      {employeeData && <EmployeeTableData employeeData={employeeData} setEmployeeData={setEmployeeData}/>}

        <div className='form-data'>
      <form onSubmit={handleSubmit} className='add-employee'>
        
        <h3>Add New Employee</h3>
        <div className='employee-form'>
        <input type="text" name="firstName" id="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} placeholder='First Name' required/>
          <input type="text" name="lastName" id="lastName"  value={lastName} onChange={(e) => setLastName(e.target.value)} placeholder='Last Name' required />
          </div>
          <br />
          <button className='add-btn'>Add Employee</button>
          
      </form>
      </div>

    </div>
  )
}

