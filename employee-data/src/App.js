import EmployeeTable from "./Components/EmployeeTable";
import Header from "./Components/Header";


function App() {
  return (
    <div >
      <Header />
      <EmployeeTable/>
    
    </div>
  );
}

export default App;
